/**
 * Main application controller
 *
 * You can use this controller for your whole app if it is small
 * or you can have separate controllers for each logical section
 *
 */
;(function() {

  angular
    .module('boilerplate')
    .controller('MainController', MainController);

  MainController.$inject = ['$scope','$location','LocalStorage', 'QueryService'];


  function MainController($scope, $location, LocalStorage, QueryService) {

    // 'controller as' syntax
    var self = this;
    $scope.username = "";
    $scope.user = false
    $scope.showNewForm = false;
    $scope.suggestionsLimit = 6;

    $scope.suggestions=["Abu Dhabi","Afghanistan","Agra","Akrotiri","Albania","Algeria","American Samoa","Amman","Amsterdam","Andorra","Angola","Anguilla","Antalya","Antarctica","Antigua and Barbuda","Argentina","Armenia","Aruba","Ashmore and Cartier Islands","Auckland","Australia","Austria","Azerbaijan","Bahamas The","Bahrain","Baku","Bangkok","Bangladesh","Barbados","Barcelona","Bassas da India","Beijing","Belarus","Belgium","Belize","Benin","Berlin","Bermuda","Bhutan","Bolivia","Bosnia and Herzegovina","Botswana","Bouvet Island","Brazil","British Indian Ocean Territory","British Virgin Islands","Brunei","Brussels","Bucharest","Budapest","Buenos Aires","Bulgaria","Burgas","Burkina Faso","Burma","Burundi","Cairo","Cambodia","Cameroon","Canada","Cancun","Cape Verde","Cayman Islands","Central African Republic","Chad","Chennai","Chiang Mai","Chile","China","Christchurch","Christmas Island","Clipperton Island","Cocos Keeling Islands","Colombia","Comoros","Congo Democratic Republic of the","Congo Republic of the","Cook Islands","Coral Sea Islands","Costa Rica","Cote d'Ivoire","Croatia","Cuba","Cyprus","Czech Republic","Delhi","Denmark","Denpasar","Dhekelia","Djerba","Djibouti","Doha","Dominica","Dominican Republic","Dubai","Dublin","East Province","Ecuador","Edirne","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Europa Island","Falkland Islands Islas Malvinas ","Faroe Islands","Fiji","Finland","Florence","France","Frankfurt","French Guiana","French Polynesia","French Southern and Antarctic Lands","Gabon","Gambia The","Gaza Strip","Georgia","Germany","Ghana","Gibraltar","Glorioso Islands","Greece","Greenland","Grenada","Guadeloupe","Guam","Guangzhou","Guatemala","Guernsey","Guilin","Guinea","Guinea-Bissau","Guyana","Haiti","Hangzhou","Hanoi","Harare","Heard Island and McDonald Islands","Ho Chi Minh City","Holy See Vatican City ","Honduras","Hong Kong","Honolulu","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Istanbul","Italy","Jaipur","Jakarta","Jamaica","Jan Mayen","Japan","Jersey","Johannesburg","Jordan","Juan de Nova Island","Kazakhstan","Kenya","Kiev","Kiribati","Kolkata","Korea North","Korea South","Kuala Lumpur","Kuwait","Kyrgyzstan","Laos","Las Vegas","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lima","Lisbon","Lithuania","London","Los Angeles","Luxembourg","Macau","Macedonia","Madagascar","Madrid","Malawi","Malaysia","Maldives","Mali","Malta","Manama","Manila","Marrakech","Marshall Islands","Martinique","Mauritania","Mauritius","Mayotte","Mecca","Melbourne","Mexico","Mexico City","Miami","Micronesia Federated States of","Milan","Moldova","Monaco","Mongolia","Montserrat","Morocco","Moscow","Mozambique","Mugla","Mumbai","Munich","Nairobi","Namibia","Nanjing","Nauru","Navassa Island","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New York City","New Zealand","Nicaragua","Nice","Niger","Nigeria","Niue","Norfolk Island","Northern Mariana Islands","Norway","Oman","Orlando","Pakistan","Palau","Panama","Papua New Guinea","Paracel Islands","Paraguay","Paris","Pattaya","Peru","Philippines","Phuket","Pitcairn Islands","Poland","Portugal","Prague","Puerto Rico","Punta Cana","Qatar","Reunion","Rio de Janeiro","Riyadh","Romania","Rome","Russia","Rwanda","Saint Helena","Saint Kitts and Nevis","Saint Lucia","Saint Pierre and Miquelon","Saint Vincent and the Grenadines","Samoa","San Francisco","San Marino","Sao Paulo","Sao Tome and Principe","Saudi Arabia","Senegal","Seoul","Serbia and Montenegro","Seychelles","Shanghai","Sharm el-Sheikh","Shenzhen","Siem Reap","Sierra Leone","Singapore","Slovakia","Slovenia","Sofia","Solomon Islands","Somalia","Sousse","South Africa","South Georgia and the South Sandwich Islands","Spain","Spratly Islands","Sri Lanka","St Petersburg","Sudan","Suriname","Suzhou","Svalbard","Swaziland","Sweden","Switzerland","Sydney","Syria","Taipei City","Taiwan","Tajikistan","Tanzania","Thailand","Timor-Leste","Togo","Tokelau","Tokyo","Tonga","Toronto","Trinidad and Tobago","Tromelin Island","Tunisia","Turkey","Turkmenistan","Turks and Caicos Islands","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States","Uruguay","Uzbekistan","Vancouver","Vanuatu","Venezuela","Venice","Vienna","Vietnam","Virgin Islands","Wake Island","Wallis and Futuna","Warsaw","Washington D C ","West Bank","Western Sahara","Yemen","Zambia","Zhuhai","Zimbabwe"
    ]

    ////////////  function definitions
    $scope.init = function(){

      $scope.path = $location.path();

      if ( LocalStorage.get('user') ){
        $scope.user = LocalStorage.get('user');

        if( $scope.path =='/friends' || $scope.path=='/user'){
          $scope.requestTravelers();
        }

        if( $scope.path=='/' || $scope.path=='/home' ){
          LocalStorage.removeAll();
        }
      }

    }

    //LOGIN
    $scope.clickLogin = function(){
      if( $scope.username !==""){
        $scope.requestLogin()
      }
      else{
        alert("Please enter a valid username");
      }
    }
    $scope.requestLogin = function(){
      if ( LocalStorage.get('user') ){
        $location.url('user');
      }
      else{
        QueryService.query('POST', 'auth', false, {"name":$scope.username}, {})
          .then(function(response) {
            $scope.user = response.data;
            //store user
            LocalStorage.set('user', response.data);

            //load user page
            $location.url('user');
          });
      }
    }

    //USER
    $scope.clickAdd = function (suggestion){
      console.log("click add")
      $scope.requestAdd(suggestion)
    }

    $scope.resetAddForm = function(){
      $scope.showNewForm = false;
      $scope.newPlaceInput='';
    }

    $scope.requestAdd = function(destination){

      var endpoint = 'travelers/'+$scope.user.id;
      //fake data
      var data = {"destinations": [{"name":destination, "visited": false}]};

      QueryService.query('PATCH', endpoint, true, {}, data)
        .then(function(response) {
          $scope.resetAddForm();
        });
    }

    $scope.requestTravelers = function(){
      QueryService.query('GET', 'travelers', true, {}, {})
        .then(function(response) {
          $scope.initPlaces(response.data);
        });
    }

    $scope.buildDestination = function(user, destination){
      return {
        userName: user.name,
        userId: user.id,
        name: destination.name,
        visited: destination.visited,
        image: destination.name.replace(' ', '')
      };

    }
    $scope.initPlaces = function(data){
      var destinationsBulk = [];
      var destinations = [];

      _.each(data, function(user){
        _.each(user.destinations, function(destination){
            destinationsBulk.push( $scope.buildDestination( user, destination ));
        })
      });

      _.each(destinationsBulk, function(destination){
        var result = _.find(destinations, {name: destination.name})

        if( result ){ //add user to already existing destination
          result.users.push( {name:destination.userName, id:destination.userId, visited:destination.visited})
        }else{
          destinations.push({ //add destination
            users: [{name:destination.userName, id:destination.userId, visited:destination.visited}],
            name: destination.name,
            image: destination.image
          })
        }
      })

      $scope.places = destinations;
    }
    $scope.init();

  }

})();
